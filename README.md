**APLICACIÓN RESERVAS DE PADEL – LARAVEL – PHP**
Aplicación web que permite añadir, modificar y eliminar reservas de padel.

**Planteamiento:**
Al realizar una reserva pedirá primero el día, a elegir en un calendario. Si el día está completo no dará opción a seleccionarlo, si no lo está, pasaremos a elegir la hora.
Una vez seleccionada la hora comprueba si esa hora ya está ocupada y muestra un aviso, sino, se realiza la reserva.
La fecha máxima para reservar serán 14 días.

Usuarios:
Se ha establecido un usuario “Administrador”, el cual puede ver todas las reservas realizadas y quién las ha realizado, pudiendo además añadir nuevas reservas, modificar las existentes y eliminarlas. El resto de usuarios tan solo podrán ver sus reservas.

Reservas:
Las reservas anteriores a la fecha de hoy no se mostrarán, pero se seguirán manteniendo en la base de datos por si hubiera que realizar una consulta para ver quienes han realizado reserva y en qué fechas. 

**APIS**:
La api de la aplicación está probada con postman y funciona correctamente. Las apis comprobadas funcionan correctamente:

- Registro (POST): 
[https://padelmontepinar.mledesma.xyz/api/register ](https://padelmontepinar.mledesma.xyz/api/register%20)
Campos: name, email, password y confirm\_password.

- Login (POST):
[https://padelmontepinar.mledesma.xyz/api/register ](https://padelmontepinar.mledesma.xyz/api/login)
Campos: email, password.

- Mostrar todas las reservas (GET):
<https://padelmontepinar.mledesma.xyz/api/books>

- Añadir nueva reserva (POST):
<https://padelmontepinar.mledesma.xyz/api/books>
Campos: fecha, hora.



