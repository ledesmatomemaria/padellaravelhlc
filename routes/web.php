<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TasksController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\BooksController;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->group(function(){
    Route::get('/dashboard',[TasksController::class, 'index'])->name('dashboard');

    Route::get('/task',[TasksController::class, 'add']);
    Route::post('/task',[TasksController::class, 'create']);

    Route::get('/task/{book}', [TasksController::class, 'edit']);
    Route::post('/task/books', [TasksController::class, 'update']);
    Route::post('/task/{book}/delete', [TasksController::class, 'update']);
    Route::post('/task/hora', [TasksController::class, 'hora']);

    Route::get('/books', [Bookscontroller::class, 'index'])->name('books');
    Route::post('/hora', [Bookscontroller::class, 'hora'])->name('hora');
    Route::post('/books', [Bookscontroller::class, 'create']);
});

Route::get('/sendemail', function () {
    try {
        $result = "Your email has been sent successfully";
        $data = array( 'name' => "LearningLaravel.net" );

        Mail::send('emails.learning', $data, function ($message) {
            $from = 'maria@mledesma.xyz';
            $name = 'maria';
            // cambiar el email y poner uno propio
            $to = 'marialedesma93@protonmail.com';
            $subject = "Learning Laravel test email";
            $message->from($from, $name);
            $message->to($to);
            $message->subject($subject);
        });
    } catch (Exception $e) {
        $result = $e->getMessage();
    }
    return $result;
});

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();
    return redirect('/dashboard');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();
    return back()->with('message', 'Verification link sent!');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');

Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware('auth')->name('verification.notice');

Route::get('/email', [EmailController::class, 'create']);
Route::post('/email', [EmailController::class, 'sendEmail'])->name('send.email');

Route::get('/', function () {
    return view('welcome');
});