<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\book;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class TasksController extends Controller
{
    public function index()
    {
        if (auth()->user()->email == 'marialedesma93@protonmail.com') {
            $books = book::all();


            //$books = auth()->user()->books();
            return view('dashboard', compact('books'));
        }
        else {
            $books = DB::table('books')->where('user_id', "=", auth()->user()->id)->get();
            //$books = auth()->user()->books();
            return view('dashboard', compact('books'));
        }

    }
    public function add()
    {
        return view('add');
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'fecha' => 'required',
            'hora' => 'hora'
        ]);
        $book = new book();
        $book->fecha = $request->fecha;
        $book->hora = $request->hora;
        $book->user_id = auth()->user()->id;
        $book->save();
        return redirect('/dashboard');
    }

    public function edit(Request $request, book $book)
    {
        $id = $book->id;
        $request->session()->put('id', $id);
        $disables = array(0);
        $books = book::all();
        foreach ($books as $book){
            $fecha = DB::table('books')->where('fecha', "=", $book->fecha)->count();
            if ($fecha == '12'){
                array_push($disables, $book->fecha);
            };
        };
        if (auth()->user()->id == $book->user_id)
        {

            return view('bookEdit', compact('book', 'disables', 'id'));
        }
        else {
            return redirect('/dashboard');
        }
    }

    public function hora(Request $request)
    {
        $book = DB::table('books')->where('id', '=', $request->session()->get('id'));
        if ($request->input('fecha')){
            $books1 = DB::table('books')->where('fecha', "=", $request->input('fecha'))->get();
            $reservas = book::all();
            $books = $reservas;
            if (count($books1) == '12')
            {
                $error = 'Este día está completo';
                return view('bookEdit')->with('error', $error);
            }
            else{
                $request->session()->put('fecha', $request->input('fecha'));
                return view('horaEdit')->with('id', $request->session()->get('id'));
            }
        }
        else {
            $error = 'No has seleccionado fecha';
            $request->session()->flash('error', $error);
            return redirect()->route('bookEdit')->with('error', $error);
        }


        $error = null;
        $error = $request->session()->get('error');
        return view('horaEdit')->with('error', $error);

    }

    public function update(Request $request, book $book)
    {
        if(isset($_POST['delete'])) {
            $book->delete();
            return redirect('/dashboard');
        }
        else {
            if ($request->input('hora')) {
                $fecha = $request->session()->get('fecha');
                $date = str_replace('/', '-', $fecha);
                $fechabuena = date("Y-m-d", strtotime($date));

                $data = DB::table('books')->where('id', "=", $request->session()->get('id'))->first();
                //$data = book::all();
                //$data->fecha = $fechabuena;
                //$data->hora = $request->input('hora');
                $book->user_id = auth()->user()->id;
                if (DB::table('books')->where('fecha', "=", $fechabuena)->where('hora', "=", $request->input('hora'))->first()) {
                    $error = 'Esa hora ya está ocupada.';
                    $request->session()->flash('error', $error);
                    return view('horaEdit')->with('error', $error);
                } else {
                    $book = DB::table('books')
                        ->where('id', $request->session()->get('id'))
                        ->update(['fecha' => $fechabuena, 'hora' => $request->input('hora')]);
                    return redirect()->route('dashboard');
                   // return $request->session()->get('id');

                };

            } else {
                $error = 'No has seleccionado hora';
                $request->session()->flash('error', $error);
                return view('horaEdit')->with('error', $error);
            }
        }
    }

    public function delete(Request $request, book $book)
    {
        if(isset($_POST['delete'])) {
            $book->delete();
            return redirect('/dashboard');
        }
        else
        {
           /* $this->validate($request, [
                'description' => 'required'
            ]);*/
            //$book->description = $request->description;
            $book->user_id = auth()->user()->id;
            $book->save();
            return redirect('/dashboard');
        }
    }
}