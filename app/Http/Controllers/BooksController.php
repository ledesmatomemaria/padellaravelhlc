<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\book;
use Illuminate\Support\Facades\DB;

class BooksController extends Controller
{
    public function index(Request $request)
    {
        $disables = array(0);
        $books = book::all();
        foreach ($books as $book){
            $fecha = DB::table('books')->where('fecha', "=", $book->fecha)->count();
            if ($fecha == '12'){
                array_push($disables, $book->fecha);
            };
        };

        $error = null;
        $error = $request->session()->get('error');

        return view('book')->with('error', $error)->with('disables', $disables);
    }
    public function create(Request $request)
    {
        if ($request->input('hora')) {
            $fecha = $request->session()->get('fecha');
            $date = str_replace('/', '-', $fecha);
            $fechabuena = date("Y-m-d", strtotime($date));

            $book = new book();
            $book->fecha = $fechabuena;
            $book->hora = $request->input('hora');
            $book->user_id = auth()->user()->id;
            if (DB::table('books')->where('fecha', "=", $book->fecha)->where('hora', "=", $book->hora)->first()){
                $error = 'Esa hora ya está ocupada.';
                $request->session()->flash('error', $error);
                return view('hora')->with('error', $error);
            }
            else if ($book->fecha < date("Y-m-d")) {
                $error = 'Ha habido un error, pruebe de nuevo';
                $request->session()->flash('error', $error);
                return redirect()->route('books')->with('error', $error);
            }
            else {
                $book->save();
                return redirect()->route('dashboard');

            };

        }
        else {
            $error = 'No has seleccionado hora';
            $request->session()->flash('error', $error);
            return view('hora')->with('error', $error);
        }
    }

    public function hora(Request $request)
    {
        if ($request->input('fecha')){
            $books1 = DB::table('books')->where('fecha', "=", $request->input('fecha'))->get();
            $reservas = book::all();
            $books = $reservas;
            if (count($books1) == '12')
            {
                $error = 'Este día está completo';
                return view('book')->with('error', $error);
            }
            else{
                $request->session()->flash('fecha', $request->input('fecha'));
                return view('hora')->with('request', $request);
            }
        }
        else {
            $error = 'No has seleccionado fecha';
            $request->session()->flash('error', $error);
            return redirect()->route('books')->with('error', $error);
        }

        $error = null;
        $error = $request->session()->get('error');
        return view('hora')->with('error', $error);

    }
}
