<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;
use App\Models\book;
use App\Http\Resources\book as TaskResource;

class TaskController extends BaseController
{

    public function index()
    {
        $books = book::where('user_id', auth()->user()->id)
            ->get();
        return $this->sendResponse(TaskResource::collection($books), 'Tasks fetched.');
    }


    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'fecha' => 'required',
        ]);

        if ($validator->fails()){
            return $this->sendError($validator->errors());
        }

        $book = new book();
        $book->fecha = $request->fecha;
        $book->hora = $request->hora;
        $book->user_id = auth()->user()->id;
        $book->save();

        return $this->sendResponse(new TaskResource($book), 'Task created.');
    }


    public function show($id)
    {
        $book = book::find($id);

        if (is_null($book)) {
            return $this->sendError('book does not exist.');
        }
        return $this->sendResponse(new TaskResource($book), 'Task fetched.');
    }


    public function update(Request $request, book $book)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'fecha' => 'required',
            'hora' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError($validator->errors());
        }

        $book->fecha = $input['fecha'];
        $book->hora = $input['hora'];
        //$task->description = $request->description;
        $book->save();

        return $this->sendResponse(new TaskResource($book), 'Book updated.');
    }

    public function destroy($id)
    {
        $book = book::find($id);
        if (is_null($book)) {
            return $this->sendError('Book does not exist.', 'Book NOT deleted.');
        }
        else {
            $book->delete();
            return $this->sendResponse([], 'Book deleted.');
        }
    }
}