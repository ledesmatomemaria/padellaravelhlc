<x-app-layout>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Mi lista de reservas') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-5">
                <div class="flex">
                    <div class="flex-auto text-2xl mb-4">Lista de reservas</div>

                    <div class="flex-auto text-right mt-2">
                        <form action="{{asset('books')}}" class="inline-block">
                            <button type="submit" name="delete" formmethod="get" class="btn btn-primary">Nueva reserva</button>
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>

                <div class="container text-center">
                    <div class="row border-black border-b-2 d-none d-md-flex">
                        @if(auth()->user()->email == 'marialedesma93@protonmail.com')
                        <div class="col-3 d-none d-md-flex text-start">
                            <h5 class="fw-bold">Usuario</h5>
                        </div>
                        @endif

                        <div class="col-6 d-none d-md-flex justify-content-start">
                            <h5 class="fw-bold">Información de la reserva</h5>
                        </div>
                        <div class="col d-none d-md-flex justify-content-end me-4">
                            <h5 class="fw-bold">Acciones</h5>
                        </div>
                    </div>

                        @foreach($books as $book)
                    <div class="row p-2">
                        @if ($book->fecha > date("Y-m-d"))
                        @if(auth()->user()->email == 'marialedesma93@protonmail.com')

                            <div class="col text-capitalize text-start my-auto">
                                  @php

                                $user = App\Models\User::find($book->user_id);
                                if (! empty($user)) {
                                  echo $user_name = $user->name; // or whatever the username field is
                                  }
                               @endphp
                            </div>
                        @endif
                            <div class="col col-md-6 text-start my-auto">
                                {{date("d/m/Y", strtotime($book->fecha))}} - {{$book->hora}}
                            </div>

                        <div class="col text-end">
                                <form action="/task/{{$book->id}}" class="inline-block">
                                    <button type="submit" name="delete" formmethod="get" class="btn btn-success my-2">Editar</button>
                                    {{ csrf_field() }}
                                </form>

                                <form action="/task/{{$book->id}}/delete" class="inline-block">
                                    <button type="submit" name="delete" formmethod="POST" class="btn btn-danger my-2">Eliminar</button>
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        @endif
                    </div>
                        @endforeach

                </div>


            </div>
        </div>
    </div>
</x-app-layout>