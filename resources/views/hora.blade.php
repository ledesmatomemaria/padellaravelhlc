<x-app-layout>
    <x-slot name="header">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Realizar una reserva') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-5">
                <div class="form-group">
                    <form action="books" method="post">
                        @csrf
                        <h2>Fecha y hora de la reserva</h2>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" id="hora" name="hora" placeholder="Hora" data-input required>
                        </div>
                        @if(isset($error))
                        <div class="alert alert-danger" role="alert">
                            {{$error}}
                        </div>
                        @endif
                        <button class="btn btn-success">Enviar</button>
                    </form>

                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

                    <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.js"></script>
                    <script>
                        $("#fecha").flatpickr({
                            dateFormat: "d/m/Y"
                        });
                        $("#hora").flatpickr({
                            enableTime: true,
                            noCalendar: true,
                            dateFormat: "H:i",
                            minuteIncrement: "0",
                            minDate: "9",
                            maxDate: "20",

                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>